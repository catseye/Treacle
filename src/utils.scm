;
; Utility functions used by Treacle
; Chris Pressey, March 2008
;

; SPDX-FileCopyrightText: (c) 2008-2024 Chris Pressey, Cat's Eye Technologies
; This file is distributed under a 2-clause BSD license.  For more information see:
; SPDX-License-Identifier: LicenseRef-BSD-2-Clause-X-Treacle

;
; Debugging output.
;
(define-syntax print
  (syntax-rules ()
    ((print e)
      (display e))
    ((print e1 e2 ...)
      (begin (display e1)
             (print e2 ...)))))

(define-syntax println
  (syntax-rules ()
    ((println e)
      (begin (display e)
             (newline)))
    ((println e1 e2 ...)
      (begin (display e1)
             (println e2 ...)))))

;
; Testing framework.
;
(define-syntax test
  (syntax-rules ()
    ((test test-name expr expected)
      (begin
        (print "Running test: " (quote test-name) "... ")
        (let ((result expr))
          (cond
            ((equal? result expected)
              (println "passed."))
            (else
              (println "FAILED!")
              (println "Expected: " expected)
              (println "Actual:   " result))))))))
